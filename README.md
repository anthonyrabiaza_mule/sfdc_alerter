# Salesforce.com Alerter (API Limits)

This is an example of SFDC API Call

## Getting Started

Please update mule-app.properties

### Prerequisites

Please make sure that you have a Application created

Setup menu>Build>Create>Apps>Connected Apps

![Alt text](src/main/resources/Connected_Apps.png?raw=true "Overview")

Connected App Name>MuleSoft

![Alt text](src/main/resources/MuleSoft_App.png?raw=true "Overview")

### Output

```
INFO  2017-06-05 17:17:11,420 [[sfdc_alerter].alerterFlow.stage1.02] org.mule.api.processor.LoggerMessageProcessor: {Max=5000000, Remaining=4999305, Percentage=0.999861}
```